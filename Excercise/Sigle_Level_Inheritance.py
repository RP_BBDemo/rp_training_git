#Single Level Inheritance

class Person(object):
    def __init__(self, name):
        self.name = name

    def getName(self):
        return self.name

    def isEmpl(self):
        return False


class Employee(Person):
    def isEmpl(self):
        return True


emp = Person("Akshay")
print(emp.getName(), emp.isEmpl())

emp = Employee("Amit")
print(emp.getName(), emp.isEmpl())
