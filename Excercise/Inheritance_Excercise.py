
class DuplicateUserError(Exception):
    pass


class UnderAgeError(Exception):
    pass


class InvalidEmail(Exception):
    pass


class User(object):
    def __init__(self, Username, EmailId):
        self.Username = Username
        self.EmailID = EmailId
        print(self.Username,self.EmailID)


ll = [("Akshay", "akshay@gmail.com", 21), ("Amit", "amit@gmail.com", 15)]

dir = {}

for usernmae, email, age in ll:
    try:
        if usernmae in dir:
            raise DuplicateUserError()
        if age < 16:
            raise UnderAgeError()

        email_pars = email.split('@')
        if len(email_pars) != 2 or not email[0] or not email_pars[1]:
            raise InvalidEmail()

    except DuplicateUserError:
        print("UserName already Exist %s " % usernmae)
    except UnderAgeError:
        print("%d is underage" % age)

    except InvalidEmail:
        print("%s is Invalid Email" % email)
    else:
        dir[usernmae]=User(usernmae,email)