import pytest


class Wallet(object):

    def __init__(self):
        self.balance = 0

    @pytest.fixture
    def empty_wallet(self):
        return Wallet.balance


def test_one(empty_wallet):
    assert empty_wallet == 0
