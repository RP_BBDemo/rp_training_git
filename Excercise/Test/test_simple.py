'''
def inc(x):
    return x + 1

def test_answer():
    assert inc(3) == 4


import pytest
def f():
    raise SystemExit(1)

def test_myTest():
    with pytest.raises(SystemExit):
        f()


class TestClass(object):
    def test_one(self):
        x="this"
        assert 'h' in x

    def test_two(self):
        x="hello"
        assert 'e' in x



def f():
    return 3


def test_function():
    assert f() == 3
import pytest


def fun():
    raise ValueError("Exception 123 raised")

def test_fun():
    with pytest.raises(ValueError,match=r'.* 123 .*'):
        fun()

'''
from test_foocompare import Foo


def pytest_assertrepr_compare(op, left, right):
    if isinstance(left, Foo) and isinstance(right, Foo) and op == "==":
        return ['Comparing Foo instances:',
                '   vals: %s != %s' % (left.val, right.val)]


class Foo(object):
    def __init__(self, val):
        self.val = val

    def __eq__(self, other):
        return self.val == other.val


def test_compare():
    f1 = Foo(1)
    f2 = Foo(2)
    assert f1 == f2
