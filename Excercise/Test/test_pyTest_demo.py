import pytest


def capital_case(x):
    return x.capitalize()


def test_capital_case():
    assert capital_case('akshay') == 'Akshay'


def test_raises_when_no_arg():
    with pytest.raises(TypeError):
        capital_case()
