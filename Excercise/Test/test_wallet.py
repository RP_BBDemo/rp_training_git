import pytest


class Wallet(Exception):




    def __init__(self):
        self.balance = 0
        print self.balance


    # def __init__(self, amt):
    #     self.balance = int(amt)

    def add_cash(self, amt):
        self.balance += int(amt)
        print self.balance

    def checkBalance(self, amt):
        try:
            if self.balance < amt:
                raise ("insufficient Balance")

        except:

            print("Insufficient Balance")

    def spend_cash(self, param):
        self.balance -= param
        print self.balance


def test_default_initial_amt():
    wallet = Wallet()
    assert wallet.balance == 0


def test_setting_initial_amt():
    wallet = Wallet()
    wallet.add_cash(100)
    assert wallet.balance == 100


def test_wallet_spend_cash():
    wallet = Wallet()
    wallet.add_cash(20)
    wallet.spend_cash(10)
    assert wallet.balance == 10


def test_wallet_exception():
    wallet = Wallet()
    wallet.checkBalance(100)


@pytest.mark.parametrize("earned,spent,expected", [(30, 10, 20), (20, 2, 18), ])
def test_transactions(earned, spent, expected):
    my_wallet = Wallet()
    print (earned,spent,expected)
    my_wallet.add_cash(earned)
    my_wallet.spend_cash(spent)
    assert my_wallet.balance == expected
