'''class Dog:

    kind = 'canine'         # class variable shared by all instances

    def __init__(self, name):
        self.name = name    # instance variable unique to each instance

# list and dictionaries are the only once which has problem of mutability in sharing variable as a class variable
'''

'''
class A:
    shared_resource=0
    tricks=[]
    namee='efe'
    def __init__(self,i):
        self.name=i
    def show(self):
        #print self.shared_resource
       # print self.tricks
        print self.namee
    def update(self,up):
        self.shared_resource=up
        self.tricks.append(up)
    def change(self,i):
        self.tricks[0]=i

    def changee(self, i):
        self.namee=i


object1 = A(1)
object1.update(5)
object2=A(1)
object2.show()
object1.show()
object1.update(66)
object1.show()
object2.show()
object1.change(22)
object1.show()
object2.show()
object1.changee("ramesh")
object1.show()
object2.show()



'''


'''
print "o1"

object1 = A(10)
object1.show()
print "o2"
object2 = A(20)
object2.show()
print "o1"
object1.show()
object2.update(50)
print "--------------"
print "o2"
object2.show()
print "o1"
object1.show()

'''
'''
object1 = A(5)
object2 = A(6)
object1.show()
object2.show()
object1.changee('piyush')
object1.show()
object2.show()
'''
 # multiple inheritance and super function.
'''

class Superparent:
    def __init__(self):
        print "In super parent"
    def show(self):
        print  "Super parent show 111 "


class Parent1(Superparent):
    def __init__(self):
        print "In  parent1"

    def show(self):
        print "1234"
        super(Parent1, self).show()


class Parent2(Superparent):
    def __init__(self):
        print "In  parent2"

    def show(self):
        print  "Super parent2 show "
        super(Parent2, self).show()


class Child(Parent1, Parent2):
    def __init__(self):
        print "In child"
    def show(self):
        print "1"
        super(Child, self).show()


obj = Child()
obj.show()


'''

#

class Cup:
    def __init__(self):
        self.color = None
        self.content = None

    def fill(self, beverage):
        self.content = beverage

    def empty(self):
        self.content = None




"""
This above code contains public class variables . Can be accessed by anyone **object. 
"""


class Cup:
    def __init__(self):
        self.color = None
        self.__content = None   # _content single underscore represents a protected type variable

    def fill(self, beverage):
        self.content = beverage

    def empty(self):
        self.content = None



c= Cup()
c.__content=5
print c.__content




'''

In Python 2, making someClass a subclass of object turns someClass into a "new-style class," whereas without (object) it's just a "classic class." See the docs or another question here for information on the differences between them; the short answer is that you should always use new-style classes for the benefits they bring.

In Python 3, all classes are "new-style," and writing (object) is redundant.



'''

