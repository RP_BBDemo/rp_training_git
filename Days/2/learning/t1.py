from typing import Union


def place(i):
    if i < 0:
        return "Hello"
    else:
        return 1


class User(object):

    def __init__(self, name):
        self.name = name


def greeting(user_name, hello):
    # type: (User, str) -> str
    print user_name
    print hello
    return hello + user_name.name


l = greeting(User("LL"), "hello")


class OrderRequest(object):
    pass


class Movie(object):
    pass


class Train(object):
    pass


def order(request):
    name = request.name()

    if request == 0:
        return Movie(name)
    else:
        return Train(name)


miMovie = {"actorName": "Lee", "production": "Disney"}


actorName = miMovie.get("actorame")

# AFP
if actorName != None:
    actorName.split()

# AFF

try:
    actorName.split()
except AttributeError :
    l = None

