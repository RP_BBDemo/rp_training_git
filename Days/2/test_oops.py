'''

class Person:
    def __init__(self, name, surname, number):
        self.name = name
        self.surname = surname
        self.number = number


class Student(Person):
    UNDERGRADUATE, POSTGRADUATE = range(2)

    def __init__(self, student_type, *args, **kwargs):
        self.student_type = student_type
        self.classes = []
        super(Student, self).__init__(*args, **kwargs)

    def enrol(self, course):
        self.classes.append(course)


class StaffMember(Person):
    PERMANENT, TEMPORARY = range(2)

    def __init__(self, employment_type, *args, **kwargs):
        self.employment_type = employment_type
        super(StaffMember, self).__init__(*args, **kwargs)


class Lecturer(StaffMember):
    def __init__(self, *args, **kwargs):
        self.courses_taught = []
        super(Lecturer, self).__init__(*args, **kwargs)

    def assign_teaching(self, course):
        self.courses_taught.append(course)


jane = Student(Student.POSTGRADUATE, "Jane", "Smith", "SMTJNX045")
jane.enrol("LG")


bob = Lecturer(StaffMember.PERMANENT, "Bob", "Jones", "123456789")
bob.assign_teaching("dee")


'''


class First(object):
    def __init__(self):
        super(First, self).__init__()
        print("first")
    def show(self):
        print("1")

class Second(object):
    def __init__(self):
        super(Second, self).__init__()
        print("second")
    def show(self):
        super(Second, self).show()
        print("2")

class Third(Second, First):
    name="piysh"
    __name=name

    def __init__(self):
        super(Third, self).__init__()
        print("Third")

    def show(self):
        super(Third, self).show()
        print ("3")


obj = Third()

obj.show()

print obj.name



