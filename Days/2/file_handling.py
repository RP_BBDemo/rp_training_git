
#directly prints lines

f = open("text.txt")
for i in f :
    print i,

#traditional way of all try catch
'''
try:
   f = open('app.log', encoding = 'utf-8')
   # do file operations.
finally:
   f.close()
   with open('app.log', encoding = 'utf-8') as f:
   #do any file operation.

'''




with open('text.txt', 'w') as f:
       # first line
    f.write('my first file\n')
       # second line
    f.write('This file\n')
       # third line
    f.write('contains three lines\n')

with open('text.txt', 'r') as f:
    content = f.readlines()

for line in content:
    print(line)



# open (filename , mode)
# f.readline() returns all the contents of a file
# f.read(number) returns the word from the start of file and increments the index where f is pointing
# f.read() returns all the remaining lines of file after the pointer location
# if the f is at the last index and if we type f.read() it returns "" empty string
# f.tell() returns the position of the f
# f.seek() returns

try :
    os.rename(cur_file, new_file)  # renaming a file
    os.remove(file_name) # removing a file
except :
    print "efefe"


f1=open("text.txt","rw+")
f1.seek(4)

print f1.tell()
print f1.read(1)
f1.write("wefw")
print "wefweff"
x=f1.read()
f1.writelines(["\'\n\'fdgd","\nretret"])

print len(x)




