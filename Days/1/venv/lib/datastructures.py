import operator

# ----------- lists ----------

# append

ll=[1,2,3]
ll.append(4)
print ll

# extend

ll=[1,2,3]
ll2=[1,2,3]
ll.extend(ll2)
print ll

# insert

ll=[1,2,3]
ll.insert(3,5)   # if index is greater than len of list
print ll        #then it directly inserts as last item on list


# remove by value

ll=[1,2,3]
ll.remove(2)
print (ll)

#pop

ll=[1,2,3]
ll.pop()
print (ll)

#index -returns first occured index of item

ll=[1,2,2,2,3]
ind=ll.index(2)
print (ind)

# count item

ll=[1,1,1,2]
print (ll.count(1))

#sort  list.sort(cmp=None, key=None, reverse=False)
# returns none

ll=[2,3,5,1]
ll.sort(reverse=False)
print (ll)
ll.sort(reverse=True)
print (ll)

# sorted - returns list sorted reversely or normally

ll=[4,5,6,1]
print (sorted(ll))

ll=[[1,2,4],[3,5,7],[2,4,5]]
print (sorted(ll,key=lambda x : x[1])) # wrt to index 1
# [[1, 2, 4], [2, 4, 5], [3, 5, 7]]
# can be applied to tuples also
# can be applied to dictionaries
dicto={'1':"piyush", '5':"sameer" , '2':"tushar"}

z= range(1,10)
print z

# 3 imp functions in lists

# FILTER

def f(x) :
    return x%3==0 or x%5==0

def f1(x,y):
    return x+y

print filter(f,range(2,25)) # returns items which is true
#[3, 5, 6, 9, 10, 12, 15, 18, 20, 21, 24]

# MAP

print map(f,range(2,25)) # returns results
#[False, True, False, True, True, False, False, True, True]

# REDUCE

print reduce(f1,range(2,25)) # its fucntion needs exactly 2 arguments
#299

# List Comprehensions
# output = [operation on x  for x in range something]
# o = [operation on x for x in range(something)]

squares= [x**2 for x in range(1,10)]
print (squares)

 #unique tuples [(x, y) for x in [1,2,3] for y in [3,1,4] if x != y]


# del keyword
# used to delete any variable from list

ll=[1,2,3,4]
del ll[0]
print ll

# zip can be used to transpose a matrix
# zip is used to convert a matrix to list of tuples

matrix =[[1,2,3],[1,2,3]]
print zip(matrix)

# ----------TUPLES ------------

#A tuple consists of a number of values separated by commas


t= 1,2,3
print t
t= t ,1,2,43
print t

# tuple unpacking is done only if the number of args are equal to variables mentioned

