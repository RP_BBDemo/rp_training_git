
"""



Python has an interesting feature called decorators to add functionality to an existing code.

This is also called metaprogramming as a part of the program tries to modify another part of the program at compile time.

"""

"""

def smart_divide(func):
    def inner(a, b):
        print("I am going to divide", a, "and", b)
        if b == 0:
            print("Whoops! cannot divide")
            return

        return func(a, b)

    return inner


# add the functionality of the  smart_divide to the divide --- pass divide to the inner

@smart_divide
def divide(a, b):
    return a / b
    
    
    
divide(3, 5)
"""





def star(func):
    def inner(a):
        print("*" * 30)
        func(a)
        print("*" * 30)
    return inner

def percent(func):
    def inner(a):
        print("%" * 30)
        func(a)
        print("%" * 30)
    return inner

@star
@percent
def printer(msg):
    print(msg)
printer("Hello")




