class Bank(object):
    def __init__(self):
        self.__rates = {}

    def reduce(self, source, string_to):
        # return Money((source.augend.amount + source.addend.amount), string_to)
        # if source.__class__.__name__=="Money":
        #     return source
        # else:
        return source.reduce(self, string_to)

    def addrate(self, from_currency, to_currency, rate):
        self.__rates.update({(from_currency, to_currency): rate})

    def getrate(self, from_currency, to_currency):
        if from_currency == to_currency:
            return 1
        return self.__rates.get((from_currency, to_currency))


class Expression(object):
    def __init__(self, augend, addend):
        self.augend = augend
        self.addend = addend

    def plus(self, addend):
        return Sums(self, addend)


class Sums(Expression):
    def __init__(self, augend, addend):
        Expression.__init__(self, augend, addend)
        # self.augend = augend
        # self.addend = addend

    def reduce(self, bank, string_to):
        # amt = self.augend.amount + self.addend.amount
        amt = self.augend.reduce(bank, string_to).amount + \
              self.addend.reduce(bank, string_to).amount
        return Money(amt, string_to)

    def times(self, multiplier):
        return Sums(self.augend.times(multiplier),
                    self.addend.times(multiplier))

    def __str__(self):
        return "augend = %s , addend = %s" % (self.augend, self.addend)


class Money(object):
    def __init__(self, amt, currency):
        self.__amount = amt
        self.__currency = currency

    @property
    def amount(self):
        return self.__amount

    @property
    def currency(self):
        return self.__currency

    @staticmethod
    def dollar(amt):
        return Money(amt, "USD")

    @staticmethod
    def rupee(amt):
        return Money(amt, "INR")

    def __str__(self):
        return "%s %s " % (self.__amount, self.__currency)

    def __eq__(self, other):
        return self.amount == other.amount and \
               self.currency == other.currency

    def __ne__(self, other):
        return self.amount != other.amount or \
               self.currency != other.currency

    def times(self, multiplier):
        return Money((self.amount * multiplier), self.currency)

    def plus(self, other):
        return Sums(self, other)
        # return Money((self.amount+other.amount), self.currency)

    def reduce(self, bank, string_to):
        rate = bank.getrate(self.currency, string_to)
        return Money(self.amount / rate, string_to)