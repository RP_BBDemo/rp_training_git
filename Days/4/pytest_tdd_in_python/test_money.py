"""Tests for Money"""

from money import *
from hamcrest import *


def test_equality():
    five_d = Money.dollar(5)
    five_rupee = Money.rupee(5)
    assert_that(Money.dollar(5), equal_to(five_d))
    assert_that(five_rupee, not_(equal_to(five_d)))


def test_times():
    five_d = Money.dollar(5)
    times_dollar = five_d.times(4)
    assert_that(times_dollar, equal_to(Money.dollar(20)))
    assert_that(five_d, Money.dollar(5))


def test_simpleaddition():
    five_d = Money.dollar(5)
    sum_money = five_d.plus(Money.dollar(10))
    bank = Bank()
    reduced = bank.reduce(sum_money, "USD")
    assert_that(Money.dollar(15), equal_to(reduced))


def test_plus_returns_sums():
    five = Money.dollar(5)
    ten = Money.dollar(10)
    result = five.plus(ten)
    assert_that(result.__class__.__name__, equal_to("Sums"))


def test_reducesum():
    sums = Sums(Money.dollar(3), Money.dollar(6))
    bank = Bank()
    result = bank.reduce(sums, "USD")
    assert_that(result, equal_to(Money.dollar(9)))


def test_reducemoney():
    bank = Bank()
    result = bank.reduce(Money.dollar(7), "USD")
    assert_that(Money.dollar(7), equal_to(result))


def test_bankrate():
    bank = Bank()
    bank.addrate("INR", "USD", 50)
    rate = bank.getrate("INR", "USD")
    assert_that(rate, equal_to(50))


def test_identity_rate():
    bank = Bank()
    rate = bank.getrate("USD", "USD")
    assert_that(rate, equal_to(1))


def test_reduce_different_currency():
    bank = Bank()
    #  {("INR","USD"):50}
    bank.addrate("INR", "USD", 50)
    result = bank.reduce(Money.rupee(50), "USD")
    assert_that(Money.dollar(1), equal_to(result))


def test_mixed_addition():
    five_d = Money.dollar(5)
    fifty_r = Money.rupee(50)
    bank = Bank()
    bank.addrate("INR", "USD", 50)
    result = bank.reduce(five_d.plus(fifty_r), "USD")
    assert_that(result, equal_to(Money.dollar(6)))


def test_sum_plus_money():
    five_d = Money.dollar(5)
    fifty_r = Money.rupee(50)
    bank = Bank()
    bank.addrate("INR", "USD", 50)
    exp = Sums(fifty_r, five_d).plus(five_d)
    result = bank.reduce(exp, "USD")
    assert_that(result, equal_to(Money.dollar(11)))


def test_sum_times_money():
    five_d = Money.dollar(5)
    fifty_r = Money.rupee(50)
    bank = Bank()
    bank.addrate("INR", "USD", 50)
    exp = Sums(fifty_r, five_d).times(2)
    result = bank.reduce(exp, "USD")
    assert_that(result, equal_to(Money.dollar(12)))