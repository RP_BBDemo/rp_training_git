def log(original_function, logfilename="log.txt"):
    def new_function(*args, **kwargs):
        with open(logfilename, "w") as logfile:
            logfile.write("Function '%s' called with positional arguments %s and keyword arguments %s.\n" % ("piyush", args, kwargs))

        return original_function(*args, **kwargs)

    return new_function

@log("someotherfilename.txt")
def my_function(message):
    print(message)