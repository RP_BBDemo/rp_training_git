def my_gen(n):
    i = 0

    while i < n:
        yield i,i**3
        i += 1

# type of my_gen is generatore object
z= my_gen(5)
print  z

for i in z:
    print i