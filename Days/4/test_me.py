


def inc(x):
    x=x+1
    return x


def test_answer():
    assert inc(4) == 5


# If expression evaluates to TRUE, assert() does nothing. If expression evaluates to FALSE, assert() displays an error message on stderr and aborts program execution.






'''

class User(object):

    def __init__(self, email, name):
        self.email_address = email
        self.name = name
        self.balance = 20

    def credit(self, amount):
        self.balance += amount


class TestME(object):

    def test_user_creation(self):
        a = User("a@b.com", "Tushar")
        assert a.email_address == "a@b.com"
        assert a.name == "Tushar"

    def test_wallet_balance(self):
        a = User("a@b.com", "Tushar")
        a.credit(20)
        assert a.balance == 20


'''
