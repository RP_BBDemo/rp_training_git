class MyClass(object):                            # executes the faster one . __slots__ reduces 50 % of ram usage as default instnce attributes are stored in the dictionaries
    __slots__ = ['name', 'identifier']
    def __init__(self, name, identifier):
        self.name = name
        self.identifier = identifier


class MyClass(object):
    #__slots__ = ['name', 'identifier']
    def __init__(self, name="wefwef", identifier="wfwe"):
        self.name = name
        self.identifier = identifier

obj = MyClass(name="piyush",identifier="dded")
print obj.name

