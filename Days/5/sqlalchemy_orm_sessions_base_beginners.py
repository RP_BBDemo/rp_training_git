from sqlalchemy import *
from sqlalchemy.orm import sessionmaker
import pdb
import sqlalchemy
from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine

from sqlalchemy import inspect


# delarative base class must contain the primary key

from sqlalchemy.ext.declarative import declarative_base
engine = create_engine('sqlite:///:memory:', echo=True)
Base = declarative_base()



# Create Session variable

"""

InstanceState.transient

InstanceState.pending

InstanceState.persistent

InstanceState.deleted

InstanceState.detached



"""
Session = sessionmaker(bind=engine)
session=Session()

# create blueprint of the database
class User1(Base):
    __tablename__ = 'users1'
    id = Column(Integer,primary_key=True)
    name = Column(String)
    fullname = Column(String(30))
    password = Column(String)

    def __repr__(self):
        return "<User(name='%s', fullname='%s', password='%s')>" % (self.name, self.fullname, self.password)


class User2(Base):
    __tablename__ = 'users2'
    id = Column(Integer,primary_key=True)
    name = Column(String)
    fullname = Column(String)
    password = Column(String)

    def __repr__(self):
        return "<User(name='%s', fullname='%s', password='%s')>" % (self.name, self.fullname, self.password)


print User1.__tablename__

print User1.__table__



# assigning to instance variables

myuser1 = User1(id=1,name="p",fullname="pn", password="piyush")
myuser2 = User1(id=4,name="p",fullname="pn", password="piyush")


# metadata.create_all(engine) is used to convert schema into actual database
# persists the data
Base.metadata.create_all(engine)


# add data to session
session.add(myuser1)
session.add(myuser2)
session.add_all([ User1(id=2,name="p",fullname="pn",password="piyush"), User1(id=5,name="p",fullname="pn",password="piyush"), User1(id=3,name="p",fullname="pn",password="piyush")])
myuser1.name="axe"
print "New ===========", session.new

print "Dirty =============", session.dirty
# python debugger uses set_trace to debug db .



# after committing if you modify the data it goes to the dirty data set
myuser2.name = "QQQ"
print "Dirty =============", session.dirty

session.close()

session.rollback()

pdb.set_trace()
